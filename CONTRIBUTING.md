# Contributing 

## Overview

Welcome to the Open Lung Open Source Ventilator Project.  We are always happy to have new contributors join us. Whether for smaller cleanups or larger proposals, we're always in need of help! The process for learning how to effectively contribute is layed out concisely on the [Contributing Wiki](https://gitlab.com/open-source-ventilator/ventilator/OpenLung/-/wikis/Contributing), it will get you up an running quickly.

