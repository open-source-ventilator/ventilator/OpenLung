# P1A1 Controller Housing - Front Panel variant for the C8A4 Mechanical Design
Mark A Lonsdale Aug 6, 2020 r1.0

---
## Design Notes / Open Issues


1.  Analog button design, resistor values and tolerances produce analog levels and tolerance defaults expected in Release 0.3  Apr 15 2020 of Arduino Controller Code OpenSourceVentilator.ino.  https://github.com/ermtl/Open-Source-Ventilator
2.  PEEP, TIDAL_VOL, RESP_RATE adjustment implemented in software via HMI in Release 0.3  Apr 15 2020 of Arduino Controller Code OpenSourceVentilator.ino.  https://github.com/ermtl/Open-Source-Ventilator
3.  EMERGENCY ON-OFF switch not implemented  in Release 0.3  Apr 15 2020 of Arduino Controller Code OpenSourceVentilator.ino. https://github.com/ermtl/Open-Source-Ventilator
4.  LCD interface is via a rear mounted I2C extension module.
5.  Release 0.3  Apr 15 2020 of Arduino Controller Code OpenSourceVentilator.ino. https://github.com/ermtl/Open-Source-Ventilator  uses I2C address of 0X27 by default. This is correct for the PCF8574 I2C extension module, but for the PCF8574A, the code address needs to be changed to 0x38.

