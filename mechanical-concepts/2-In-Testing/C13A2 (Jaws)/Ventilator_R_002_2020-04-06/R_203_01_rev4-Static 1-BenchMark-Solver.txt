Solver 5.787 Linear-static-solver-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 5.788 Loading-and-preparing-input-data-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 6.082 Loading-and-preparing-input-data-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 6.082 Computing-load-vector-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 6.415 Computing-load-vector-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 6.526 FFEPlus-Solver-start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 9.018 FFEPlus-Solver-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 9.362 Computing-stresses-for-load-case=1-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 9.366 Computing-reaction-forces-for-load-case(LCASE)=1-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 10.023 Computing-reaction-forces-for-load-case(LCASE)=1-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 10.037 Computing-stresses-for-load-case=1-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 10.114 Linear-static-solver-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
SOLVER TYPE= FFEPlus (PCG Iterative Solver)
Number of Equations (NEQ)=63279
Number of Nodal points (NUMNP)=21225
Number of Pseudo nodes (NUMPS)=0
Number of Elements (NUME)=12862
Number of Pseudo Elements (NUMP)=0
SOLUTION TIME LOG
Time for Input Data Transfer from Database =1.185000
TIME FOR CALCULATION OF STRUCTURE STIFFNESS MATRIX (STF_PHASE)=0.018000
TIME FOR FFEPlus (PCG ITERATIVE SOLVER) =2.609000
TIME FOR GAP & CONTACT ITERATIONS (CNT_PHASE)=0.000000
TIME FOR REACTION FORCE & DISP UPDATE TO POST (REC_PAHS)=0.341000
TIME FOR STRESS CALCULATION (STS_PHASE)=0.675000
TIME FOR WRITING POST PROCESSING INFORMATION (PST_PHASE)=0.016000
TOTAL SOLUTION TIME (TOT_TIME)=4.806000
